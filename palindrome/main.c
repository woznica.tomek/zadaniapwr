#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../interfaces/forward_list/list.h"


void copy(void* a, void* b, size_t elem_size){

    memcpy(a, b, elem_size);

}

int compare(void * a, void * b){

    return (*(int*)a - *(int*)b);

}

void visit(void * storage){

    printf("%i, ", *(int*)storage);

}

int isPalindrome(List list){

    List reversedList = listReverse(list);

    int res = (listCompare(list, reversedList) == 1);

    listDestroy(&reversedList);

    return res;

}


int main(int argc, char* argv[]){

    List list;
    listInitialize(&list, compare, copy, sizeof(int));

    int tab[] = {1,2,3,3,2,1};
    for (int i = 0; i < sizeof(tab) / sizeof(int); ++i){

        listInsert(&list, &tab[i]);

    }

    listTraverse(list, visit); printf("\n");

    int what = 5;

    void * res = listFind(&list, (void*)&what);

    if (res != NULL){

        printf("OK\n");

    }else{

        printf("Not found\n");

    }

    List newList = copyList(list);
    listTraverse(newList, visit);printf("\n");

    List reversedList = listReverse(newList);
    listTraverse(reversedList, visit); printf("\n");

    printf("%i\n", listCount(reversedList));

    printf("%i\n", listCompare(list, reversedList));

    printf("List is palindrome : %i\n", isPalindrome(list));

    listRemove(&list, (void*)&tab[1]);
    listTraverse(list, visit); printf("\n");

    listDestroy(&reversedList);
    listDestroy(&newList);
    listDestroy(&list);
    
    return 0;

}
