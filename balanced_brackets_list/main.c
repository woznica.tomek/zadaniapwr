#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../interfaces/stack/stack.h"

// for test purposes
int checkBalanced(char * str, size_t size){

  char openBrackets[] = {'(','{','['};
  char closedBrackets[] = {')','}',']'};

  StackType stack;

  stackInitialize(&stack);

  for (int i = 0; i < size; ++i)
  {
    for(int j = 0; j < sizeof(openBrackets) / sizeof(char); ++j){

      if (str[i] == openBrackets[j]){

        stackPush((void*)&j, sizeof(int), &stack);

      }else if (str[i] == closedBrackets[j]){

        if (!stackEmpty(stack)){
          
          int * openBracketIndex = (int*)stackTop(stack);
          if (str[i] == closedBrackets[*openBracketIndex]){
            
            stackPop(&stack);
            
          }else{

            while (!stackEmpty(stack)){

              stackPop(&stack);
            }
            
            return 0;

          }

        }

      }

    }

  }


  if (stackEmpty(stack)) return 1;

  return 0;

}

int main(int argc, char* argv[]){
    
    /*char * test = "([])(){}(())()()";
    char * test1 = "(())()";
    char * test2 = "()[]{}{";
    char * test3 = "()()[{()})]";
    char * test4 = "((){{{{[]}}}})()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()";*/


    #define TestVal 1000000

    char * test5 = (char*)malloc(sizeof(char) * TestVal);

    char brackets[] = {'[', ']', '(', ')', '{', '}'};

    //int size = sizeof(brackets) / sizeof(char);

    for (long i = 0; i < TestVal / 2; ++i){

        test5[i] = brackets[0];

    }

    for (long i = TestVal / 2; i < TestVal; ++i){

        test5[i] = brackets[1];

    }
    int balanced = checkBalanced(test5, TestVal - 1);
    printf("%i\n", balanced);

    free(test5);

    return 0;

}
