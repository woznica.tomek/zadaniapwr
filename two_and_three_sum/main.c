#include <stdio.h>
#include <stdlib.h>

#include "../interfaces/hash_table/hash_table.h"
#include "../interfaces/dynamic_array/array.h"


//runs in quadratic time
int * sumOfTwoNaive(int * inputArray, int targetSum){


    int *result = NULL;

    int inputArrayLength = array_length(inputArray);

    for (int i = 0; i < inputArrayLength; ++i){

        for (int j = 1; j < inputArrayLength; ++j){

            if (inputArray[i] != inputArray[j]){

                    int sum = inputArray[i] + inputArray[j];

                    if (sum == targetSum){
                    array_push(result, inputArray[i]);
                    array_push(result, inputArray[j]);
                    return result;
                }

            }

        }

    }

    return result;

}

//runs in linear time

int * sumOfTwo(int * inputArray, int targetSum){

    int * result = NULL;

    int inputArrayLength = array_length(inputArray);

    for (int i = 0; i < inputArrayLength; ++i){

        insert(inputArray[i], i);


    }

    for (int i = 0; i < inputArrayLength; ++i){

        int tmp = targetSum - inputArray[i];

        struct DataItem * searchRes = search(tmp);

        if( searchRes != NULL && searchRes->data != i){

            array_push(result, inputArray[i]);
            array_push(result, tmp);
            return result;

        }

    }


    return result;
}


int main (int argc, char * argv[]){

  int test[] = {3, 5, -4, 8, 11, 1, -1, 6};

  int * inputArray = NULL;

  for (int i = 0; i < sizeof(test) / sizeof(int); ++i){

      array_push(inputArray, test[i]);


  }



  int * result = sumOfTwoNaive(inputArray, 10);

  int resultLength = array_length(result);

  for (int i = 0 ; i < resultLength; ++i){

      printf("%i," , result[i]);
  }

  printf("\n");

  array_free(result);
//try again with new 
  result = NULL;

  result = sumOfTwo(inputArray, 10);

  resultLength = array_length(result);

  for (int i = 0 ; i < resultLength; ++i){

      printf("%i," , result[i]);
  }

  printf("\n");

  array_free(inputArray);
  array_free(result);
  

  return EXIT_SUCCESS;

}

