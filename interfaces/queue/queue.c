#include "queue.h"

void initQ(QType Q){

    Q[0].first = Q[0].last = -1;

}

int Qfull(QType Q){

    return (Q[0].first == 0 && Q[0].last == QMAX - 1) || (Q[0].first == Q[0].last + 1);

}

int Qempty(QType Q){

    return Q[0].first == -1;

}

int enq(void* el, QType Q){

    if (!Qfull(Q)){

        if (Q[0].last == QMAX - 1 || Q[0].last == -1){

            Q[0].storage[0] = el;
            Q[0].last = 0;
            if (Q[0].first == -1) Q[0].first = 0;

        }else{
       
            Q[0].storage[++Q[0].last] = el;

        }

        return QNO_ERROR;

    }else{
      
        printf("Queue overflow\n");
        return QFULL;
    }

}

void* deq(QType Q){

    void* retVal;

    if(!Qempty(Q)){
        
        retVal = Q[0].storage[Q[0].first];
        if(Q[0].first == Q[0].last){

            Q[0].last = Q[0].first = -1;


        }else if (Q[0].first == QMAX -1 ){


            Q[0].first = 0;            


        }else{

            Q[0].first++;

        }
        return retVal;
    }else{

        printf("Queue is Qempty\n");
      return NULL;

    }


}
