#ifndef QUEUE_H
#define QUEUE_H
#include <stdio.h>
#include <stdlib.h>

#define QMAX 50
#define QFULL 0xABCD
#define QEMPTY 0xABC
#define QNO_ERROR 0xAB

struct QStorage{
    int first, last;
    void* storage[QMAX];
};


typedef struct QStorage QType[1];

void initQ(QType Q);
int Qfull(QType Q);
int Qempty(QType Q);
int enq(void* el, QType Q);
void* deq(QType Q);

#endif
