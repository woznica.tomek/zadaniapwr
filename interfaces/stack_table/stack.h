#ifndef STACK_H
#define STACK_H


#define STACK_OK 0xAB
#define STACK_OVERFLOW 0xABC
#define STACK_EMPTY 0xABCD


#define STACK_SIZE 2048 * 2048

struct StackRec{
    void * storage[STACK_SIZE];
    int firstIndex;
    int lastIndex;
};

typedef struct StackRec * StackType;


void stackInitialize(StackType * stack);
int stackEmpty(StackType stack);
int stackPush(void * el, size_t nbrOfBytes, StackType * stack);
void stackPop(StackType * stack);
void * stackTop(StackType stack);
void stackDestroy(StackType * stack);

#endif
