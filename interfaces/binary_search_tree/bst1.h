#ifndef BST_H
#define BST_H
//typedef int eltype;



struct node_record{

    void * key;
    struct node_record *left, *right;
};

typedef struct node_record *tree_type;

struct treeInfo{

   tree_type root;
   int (*compare)(void*, void*);
   void (*copy)(void*, void*, size_t);

};


typedef struct treeInfo* Tree;

void initTree(Tree * tree, int (*compare)(void*, void*), void (*copy)(void*, void*,size_t));
tree_type search(Tree tree, void* key);
void insert(Tree* tree, void* key, size_t elem_size);
void delete_by_copying(tree_type *node);
void delete_by_merging(tree_type *node);
void delete(Tree* tree, void* key);
void breadth_first(tree_type node);
void preorder(tree_type node);
void inorder(tree_type node);
void postorder(tree_type node);
#endif
