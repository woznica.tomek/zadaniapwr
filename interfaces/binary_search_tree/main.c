#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "bst1.h"


void copy(void * a, void * b, size_t num){
  
    memcpy(a,b,num);

}

int compare(void * a, void * b){

    return (*(int*)a - *(int*)b);

}


int main(int argc, char* argv[]){
    
    int tab[] = {1,5,2,8,7,3};
    size_t tabSize = sizeof(tab) / sizeof(int);

    Tree tree = (Tree)malloc(sizeof(struct treeInfo));
    initTree(&tree, compare, copy);

    for(int i = 0; i < tabSize; ++i){
      
        insert(&tree, (void*)&tab[i], sizeof(int));

    }

    breadth_first(tree->root);printf("\n");
    inorder(tree->root);printf("\n");
    preorder(tree->root);printf("\n");
    postorder(tree->root);printf("\n");

    int val = 5;
    if (search(tree, (void*)&val) != NULL){

      printf("OK, found = %i\n", val);

    } else{

      printf("Val = %i not found\n", val);

    }

    delete(&tree, (void*)&val);

    if (search(tree, (void*)&val) != NULL){

      printf("OK, found = %i\n", val);

    } else{

      printf("Val = %i not found\n", val);

    }

    breadth_first(tree->root);printf("\n");
    inorder(tree->root);printf("\n");
    preorder(tree->root);printf("\n");//moving
    postorder(tree->root);printf("\n");


    return 0;

}
