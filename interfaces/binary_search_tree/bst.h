#ifndef BST_H
#define BST_H
typedef int eltype;

struct node_record{

    eltype key;
    struct node_record *left, *right;
};


typedef struct node_record *tree_type;

tree_type search(tree_type node, eltype key);
void insert(tree_type * root_addr, eltype key){
void delete_by_copying(tree_type *node);
void delete_by_merging(tree_type *node);
void delete(tree_type *root_addr, eltype key);
void breadth_first(tree_type node);
void preorder(tree_type node);
void inorder(tree_type node);
void postorder(tree_type node);
#endif
