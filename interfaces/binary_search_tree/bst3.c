#include <stdio.h>
#include <stdlib.h>

#include "bst1.h"
#include "../queue/queue.h"



void preorder(tree_type node){

    if (node){

       printf("%i, ", *(int*)node->key);
       preorder(node->left);
       preorder(node->right);

    }
}
void inorder(tree_type node){

    if (node){

        inorder(node->left);
        printf("%i, ", *(int*)node->key);
        inorder(node->right);
    }

}
void postorder(tree_type node){

    if (node){
        postorder(node->left);
        postorder(node->right);
        printf("%i, ", *(int*)node->key);
    }

}


void breadth_first(tree_type node){

    QType Q;

    if (node){

       initQ(Q);
       enq((void*)node, Q);
       while(!Qempty(Q)){

           tree_type node = (tree_type)deq(Q);
           printf("%i, ",*(int*)node->key);
           if (node->left) enq((void*)node->left, Q);
           if (node->right) enq((void*)node->right, Q);

       }
        

    }

}

tree_type search(Tree tree, void* key){

    tree_type node = tree->root;

    while(node){

        if (tree->compare(node->key, key) == 0){
            
            return node;

        }else if (tree->compare(node->key, key) > 0){

            node = node->left;

        }else{

            node = node->right;

        }
    }
    return NULL;
}


void initTree(Tree * tree, int (*compare)(void*, void*), void (*copy)(void*, void*,size_t)){

      (*tree)->compare = compare;
      (*tree)->root = NULL;
      (*tree)->copy = copy;

}

void insert(Tree * tree, void * key, size_t elem_size){

    tree_type * root_addr = &(*tree)->root;
    
    tree_type p = (*tree)->root;
    
    tree_type previous = NULL;
    tree_type new_node = (tree_type)malloc(sizeof(struct node_record));
    new_node->left = new_node->right = NULL;
    new_node->key = malloc(elem_size);
    (*tree)->copy(new_node->key, key, elem_size);
    
    while(p){
    
        previous = p;
        
        if ((*tree)->compare(p->key, key) > 0){
            p = p->left;
        }else{
            p = p->right;
        }
    }
    
    if (!*root_addr){

        *root_addr = new_node;

    }else if ((*tree)->compare(previous->key, key) > 0){
        
        previous->left = new_node;
    }else{

        previous->right = new_node;

    }


}
// remove algorithm improved by Knuth
void delete_by_copying(tree_type *node){

    tree_type previous = *node;
    tree_type tmp = *node;

    if (!(*node)->right){
        
        *node = (*node)->left;

    }else if (!(*node)->left){
        
        *node = (*node)->right;

    }else{
        
        tmp = (*node)->left;
        previous = *node;
        while (tmp->right){

           previous = tmp;
           tmp = tmp->right;

        }
        (*node)->key = tmp->key;
        if (previous == *node){
            previous->left = tmp->left;
        }else{
            previous->right = tmp->left;
        }
    }

    free(tmp); 
}
//basic algorithm to remove node
void delete_by_merging(tree_type *node){

    tree_type tmp = *node;

    if (*node){

        if (!(*node)->right){

            *node = (*node)->left;

        }else if (!(*node)->left){

            *node = (*node)->right;
        }else{

            tmp = (*node)->left;
            while (tmp->right){
                
                tmp = tmp->right;
            }
            tmp->right = (*node)->right;
            tmp = *node;
            *node = (*node)->left;
        }
    
        free(tmp);
    }


}

void delete(Tree *  tree, void* key){

    tree_type * root_addr = &(*tree)->root;
    tree_type node = *root_addr;
    tree_type previous = NULL;

    if (!(*root_addr)){

        printf("Tree is empty\n");
        return;
    }

    while(node){

        if ((*tree)->compare(node->key, key) == 0) break;

        previous = node;
        if ((*tree)->compare(node->key, key) > 1){

            node = node->left;

        }else{

            node = node->right;

        }
    }

    if(node){

        if (node == *root_addr){

            delete_by_merging(root_addr);

        }else if (previous->left == node){

            delete_by_merging(&previous->left);    

        }else{

            delete_by_merging(&previous->right);

        }
        
    }else{

        printf ("Node does not exist\n");

    }
}
    

